const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const blogSchema = new Schema({
  title:  String,
  author: String,
  body:   String,
  comments: [{ body: String, date: Date }],
  public: { type: Boolean, default: false },
  file: { type: String, required: false },
  deleted: {type: Boolean, default: false},
  date: { type: Date, default: Date.now },

  sharedWith: [Number]
});

module.exports = mongoose.model('blog', blogSchema);