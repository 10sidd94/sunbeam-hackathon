from flask import Flask, render_template, request, jsonify
from DB import Database
from flask_cors import CORS

db = Database()


def format_result(error, data=None):
    result = {}
    if error:
        result['status'] = 'error'
        result['error'] = error
    else:
        result['status'] = 'success'
        result['data'] = data

    return result


app = Flask(__name__)

CORS(app)

@app.route('/')
def root():
    return render_template("index.html")


@app.route('/user/all')
def get_users():
    my_id = request.args['myId']
    statement = f"select id, firstName, lastName, email from user where id <> {my_id}"
    result = db.dcl(statement)
    users = []
    for user in result:
        users.append({
            "id": user[0],
            "name": f"{user[1]} {user[2]}",
            "email": user[3]
        })

    return format_result(None, users)


@app.route('/user/register', methods=["POST"])
def register_user():
    first_name = request.json.get('firstName')
    last_name = request.json.get('lastName')
    email = request.json.get('email')
    password = request.json.get('password')
    phone = request.json.get('phone')

    statement = f"insert into user (firstName, lastName, email, password, phone) values ('{first_name}', '{last_name}', '{email}', '{password}', '{phone}')"
    db.dml(statement)
    return format_result(None, "user registered")


@app.route('/user/login', methods=["POST"])
def login_user():
    email = request.json.get('email')
    password = request.json.get('password')

    statement = f"select id, firstName, lastName, email from user where email= '{email}' and password = '{password}'"
    result = db.dcl(statement)
    if len(result) == 0:
        return format_result("user not exist")
    else:
        user = result[0]
        return format_result(None, {
            "id": user[0],
            "name": f"{user[1]} {user[2]}",
            "email": user[3]
        })


app.run(port=4000, host='0.0.0.0', debug=True)